from flask import render_template, flash, redirect, url_for
from flask.ext.login import login_required, current_user, login_user, login_required, logout_user
from .. import db
from ..models import pg_details, User,sharing_options

from forms import search_form, add_partner, partner_login, add_pg,Edit_User
from . import customer

@customer.route('/',methods = ['GET','POST'])
@customer.route('/index',methods = ['GET','POST'])
@customer.route('/index.html',methods = ['GET','POST'])
def index():
    loc = None
    form = search_form()
    #http://stackoverflow.com/questions/25488861/selectfield-option-to-dynamically-populate-default-value-in-textareafield-using
    form.locality.choices = [(pg.id, pg.locality) for pg in \
                               pg_details.query.filter(pg_details.active == True).all()]
    if form.validate_on_submit():
        #
        for choice_id,choice_locality in form.locality.choices:
            if choice_id == form.locality.data:
                loc = str(choice_locality).strip()

        print "location",loc
        pgs = pg_details.query.filter(pg_details.active == True).filter(pg_details.serving_gender==form.gender.data)\
                                    .filter(pg_details.locality==loc).all()
        for pg in pgs:
            print pg.locality

        print "all pgs ", pgs
        return render_template('customer_search_results.html', pgs=pgs, serving_gender=form.gender.data, locality= loc)
    return render_template('index.html', form=form )



@customer.route('/new-partner',methods = ['GET','POST'])
def new_partner():
    form = add_partner()
    if form.validate_on_submit():
        partner_email_from_db = User.query.filter_by(email=form.email.data).first()
        if partner_email_from_db is None:
            partner = User(email = form.email.data, number = form.number.data,
                                  password = form.password.data, accept_tos = form.accept_tos.data)
            db.session.add(partner)
            db.session.commit()
            flash('This profile is registered, please login')
            return redirect(url_for('.login_partner'))
        else:
            flash('This profile already exists, please login')
            return redirect(url_for('.login_partner'))

    return render_template('new-partner.html', form = form)


@customer.route('/partner-login',methods = ['GET','POST'])
def login_partner():
    form = partner_login()
    if form.validate_on_submit():
        partner_email_from_db = User.query.filter_by(email=form.email.data).first()
        if partner_email_from_db is None:
            flash('This email does not exists, please register')
            return redirect(url_for('.new_partner'))
        elif not partner_email_from_db.verify_password(form.password.data):
            flash("invalid password")
            form.email = form.email.data
            return render_template('partner-login.html', form = form)
        else:
            login_user(partner_email_from_db)
            return redirect (url_for('.show_my_pgs'))
    return render_template('partner-login.html', form = form)

@customer.route('/add-new-pg',methods = ['GET','POST'])
@login_required
def add_new_pg():
    form = add_pg()

    if form.validate_on_submit():
        new_pg = pg_details(name = form.name.data, locality = form.locality.data,
                            serving_gender = form.serving_gender.data, phone_number = form.phone_number.data,
                            wifi = form.wifi.data, security = form.security.data, reception = form.reception.data,
                            cctv = form.cctv.data, refrigerator = form.refrigerator.data,
                            drinking_water = form.drinking_water.data, lift = form.lift.data,
                            parking = form.parking.data, power_backup= form.power_backup.data,
                            tv = form.tv.data, almirah = form.almirah.data, house_keeping = form.house_keeping.data,
                            hot_water = form.hot_water.data, pillow = form.pillow.data, laundry = form.laundry.data,
                            breakfast = form.breakfast.data, lunch = form.lunch.data, dinner = form.dinner.data,
                            description = form.description.data, partner_id=current_user.id)

        db.session.add(new_pg)
        db.session.commit()
        print new_pg
        flash('The PG was added successfully.')

        return render_template('add-new-pg.html', form = form)
    return render_template('add-new-pg.html', form = form)


@customer.route('/modify-pg',methods=['GET','POST'])
@login_required
def modify_pg():
    form = add_pg()
    if form.validate_on_submit():
        pass
    return render_template('modify-pg.html', form = form)

@customer.route('/logout/')
@login_required
def logout():
    logout_user()
    flash("You are now logged out")
    return redirect(url_for('.index'))

@customer.route('/edit-profile/',methods=['GET','POST'])
@login_required
def edit_profile():
    form = Edit_User()
    if form.validate_on_submit():
        current_user.email = form.email.data
        current_user.number = form.number.data
        current_user.password = form.password.data
        db.session.add(current_user._get_current_object())
        db.session.commit()
        flash("Your profile is updated!")
        return redirect(url_for('.index'))
    form.email.data = current_user.email
    form.number.data = current_user.number
    return render_template('edit_partner.html',form=form)



@customer.route('/show-my-pgs/',methods=['GET','POST'])
@login_required
def show_my_pgs():
    my_pgs = pg_details.query.filter_by(partner_id=current_user.id)
    return render_template('show_my_pgs.html',my_pgs=my_pgs)


@customer.route('/terms/')
def terms():
    return render_template('terms_and_conditions.html')