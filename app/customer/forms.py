from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField, SubmitField, SelectField, PasswordField, BooleanField, TextField,validators
from wtforms.fields.html5 import DateField
from wtforms.validators import Optional, Length, Required, URL, Email
from ..models import pg_details

class search_form(Form):

    locality = SelectField('Locality', coerce=int)
    #locality = SelectField('Locality', choices=[('btm',"BTM Layout, Bangalore"),
     #                                                  ('koramangala',"Koramangala, Bangalore"),
      #                                                 ('hsr',"HSR Layout, Bangalore")])

    gender = SelectField('Gender',choices = [('women','women'),('men','men')])

    submit = SubmitField('Search')

class add_partner(Form):
    email = StringField('Email id',validators = [Required(), Email()],description="test")
    number = TextField('Phone Number', validators = [Required()])
    password = PasswordField('New Password', [validators.Required(),validators.EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Repeat Password')

    accept_tos = BooleanField('I accept the TOS', [validators.Required()])
    submit = SubmitField('Submit')

class partner_login(Form):
    email = StringField('Email id',validators = [Required(), Email()])
    password = PasswordField('Password')
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Log In')


class add_pg(Form):
    name = StringField('Name of the PG',validators = [Required()])
    locality = StringField('Locality', validators = [Required()])
    serving_gender = SelectField('Serving Gender',choices=[('men','men'),('women','women')])

    phone_number = TextField('Phone Number')
    wifi = BooleanField('Wifi')
    security = BooleanField('Security')
    reception = BooleanField('Reception')
    cctv = BooleanField('CCTV')
    refrigerator = BooleanField('Refrigerator')
    drinking_water = BooleanField('Drinking Water')
    lift = SelectField('Lift', choices=[('yes',"Yes"), ('no','No'),('not_required', 'Not Required')])
    parking = SelectField('Parking', choices=[('4_wheeler',"4 Wheeler (includes 2 wheeler)"), ('2_wheeler','Only 2 Wheeler'),
                                              ('no_parking','No Parking')],validators = [Required()])
    power_backup = BooleanField('Power Backup')
    tv = BooleanField('TV')
    almirah = BooleanField('Almirah')
    house_keeping = BooleanField('House keeping')
    hot_water = BooleanField('Hot Water Supply')
    pillow = BooleanField('Pillow')
    laundry = BooleanField('Laundry')
    breakfast = SelectField('Breakfast', choices=[('yes',"Yes"), ('no','No'),
                                              ('on_request','available_on_request')],validators = [Required()])
    lunch = SelectField('Lunch', choices=[('yes',"Yes"), ('no','No'),
                                              ('on_request','available_on_request')],validators = [Required()])
    dinner = SelectField('Dinner', choices=[('yes',"Yes"), ('no','No'),
                                              ('on_request','available_on_request')],validators = [Required()])

    description = TextAreaField('Add Description (Why should one select your PG)')

    active = BooleanField('Active Listing')
    submit = SubmitField('Add')

class Edit_User(Form):
    email = StringField('Email id',validators = [Required(), Email()])
    number = TextField('Phone Number', validators = [Required()])


    password = PasswordField('New Password', [validators.Required(),validators.EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Repeat Password')
    submit = SubmitField('Save')



class Edit_Pg(Form):
    name = StringField('Name of the PG',validators = [Required()])
    locality = StringField('Locality', validators = [Required()])
    serving_gender = SelectField('Serving Gender',choices=[('male','Male'),('female','Female')])

    phone_number = TextField('Phone Number')
    wifi = BooleanField('Wifi')
    security = BooleanField('Security')
    reception = BooleanField('Reception')
    cctv = BooleanField('CCTV')
    refrigerator = BooleanField('Refrigerator')
    drinking_water = BooleanField('Drinking Water')
    lift = SelectField('Lift', choices=[('yes',"Yes"), ('no','No'),('not_required', 'Not Required')])
    parking = SelectField('Parking', choices=[('4_wheeler',"4 Wheeler (includes 2 wheeler)"), ('2_wheeler','Only 2 Wheeler'),
                                              ('no_parking','No Parking')],validators = [Required()])
    power_backup = BooleanField('Power Backup')
    tv = BooleanField('TV')
    almirah = BooleanField('Almirah')
    house_keeping = BooleanField('House keeping')
    hot_water = BooleanField('Hot Water Supply')
    pillow = BooleanField('Pillow')
    laundry = BooleanField('Laundry')
    breakfast = SelectField('Breakfast', choices=[('yes',"Yes"), ('no','No'),
                                              ('on_request','available_on_request')],validators = [Required()])
    lunch = SelectField('Lunch', choices=[('yes',"Yes"), ('no','No'),
                                              ('on_request','available_on_request')],validators = [Required()])
    dinner = SelectField('Dinner', choices=[('yes',"Yes"), ('no','No'),
                                              ('on_request','available_on_request')],validators = [Required()])

    description = TextAreaField('Add Description (Why should one select your PG)')

    active = BooleanField('Active Listing')
    submit = SubmitField('Save')