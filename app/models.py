from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask import request
from flask.ext.login import UserMixin
from . import db, login_manager


class User(UserMixin, db.Model):
    __tablename__='users'
    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.String(64), nullable=False)
    email = db.Column(db.String(512), nullable=True)
    partner_password = db.Column(db.String(128),nullable=False)
    accept_tos =db.Column(db.Boolean, nullable=False)
    is_admin = db.Column(db.Boolean,default=False)

    def __init__(self,**kwargs):
        super(User,self).__init__(**kwargs)

    @property
    def password(self):
        raise AttributeError('Password is not readable attribute')

    @password.setter
    def password(self,password):
        self.partner_password = generate_password_hash(password)

    def verify_password(self,password):
        return check_password_hash(self.partner_password,password)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class pg_details(db.Model):
    __tablename__ = 'pg_details'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(512),nullable=False)
    locality = db.Column(db.String(512), nullable=False)
    serving_gender = db.Column(db.String(16), nullable=False)

    phone_number = db.Column(db.String(64))
    wifi=db.Column(db.Boolean,nullable=False)
    security = db.Column(db.Boolean,nullable=False)
    reception = db.Column(db.Boolean,nullable=False)
    cctv = db.Column(db.Boolean,nullable=False)
    refrigerator = db.Column(db.Boolean,nullable=False)
    drinking_water = db.Column(db.Boolean,nullable=False)
    lift = db.Column(db.String(24),nullable=False)
    parking = db.Column(db.String(24),nullable=False)
    power_backup = db.Column(db.Boolean,nullable=False)
    tv = db.Column(db.Boolean,nullable=False)
    laundry = db.Column(db.Boolean,nullable=False)
    hot_water = db.Column(db.Boolean,nullable=False)
    almirah = db.Column (db.Boolean,nullable=False)
    house_keeping = db.Column(db.Boolean,nullable=False)
    pillow = db.Column(db.Boolean, default=True)
    breakfast = db.Column(db.String(24))
    lunch = db.Column(db.String(24))
    dinner = db.Column(db.String(24))
    description = db.Column(db.String(1024))
    date_of_addition = db.Column(db.DateTime(), default=datetime.utcnow)
    date_of_last_modification = db.Column(db.DateTime(), default=datetime.utcnow)
    active = db.Column(db.Boolean,default=True)
    partner_id = db.Column(db.Integer,db.ForeignKey('users.id'))

class sharing_options(db.Model):
    __tablename__ = 'sharing_options'
    id = db.Column(db.Integer, primary_key=True)
    pg_id = db.Column(db.Integer,db.ForeignKey('pg_details.id'))
    four_people_sharing_rate = db.Column(db.String(512),nullable=True)
    four_people_sharing_deposit = db.Column(db.String(512),nullable=True)

    three_people_sharing_rate = db.Column(db.String(512),nullable=True)
    three_people_sharing_deposit = db.Column(db.String(512),nullable=True)

    two_people_sharing_rate = db.Column(db.String(512),nullable=True)
    two_people_sharing_deposit = db.Column(db.String(512),nullable=True)

    individual_room_rate = db.Column(db.String(512),nullable=True)
    individual_room_deposit = db.Column(db.String(512),nullable=True)

class nearby_places(db.Model):
    __tablename__ = 'nearby_places'
    id = db.Column(db.Integer, primary_key=True)
    pg_id = db.Column(db.Integer,db.ForeignKey('pg_details.id'))

    place1_name = db.Column(db.String(512),nullable=True)
    place1_distance = db.Column(db.String(512),nullable=True)
    place1_time = db.Column(db.String(512),nullable=True)


    place2_name = db.Column(db.String(512),nullable=True)
    place2_distance = db.Column(db.String(512),nullable=True)
    place2_time = db.Column(db.String(512),nullable=True)


    place3_name = db.Column(db.String(512),nullable=True)
    place3_distance = db.Column(db.String(512),nullable=True)
    place3_time = db.Column(db.String(512),nullable=True)


    place4_name = db.Column(db.String(512),nullable=True)
    place4_distance = db.Column(db.String(512),nullable=True)
    place4_time = db.Column(db.String(512),nullable=True)


    place5_name = db.Column(db.String(512),nullable=True)
    place5_distance = db.Column(db.String(512),nullable=True)
    place5_time = db.Column(db.String(512),nullable=True)


    place6_name = db.Column(db.String(512),nullable=True)
    place6_distance = db.Column(db.String(512),nullable=True)
    place6_time = db.Column(db.String(512),nullable=True)